import { PureComponent } from 'react';
import PropTypes from 'prop-types';

import Header from '../../components/header';
import '../../assets/less/normalize.less';
import '../../assets/less/global.less';

export default class App extends PureComponent {
  static propTypes = {
    children: PropTypes.node,
  };

  render() {
    return (
      <div>
        <Header/>
        {this.props.children}
      </div>
    );
  }
}
