import { PureComponent } from 'react';
import { connect } from 'react-redux';

class Home extends PureComponent {
  render() {
    return (
      <div>
        {'home'}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  home: state.home,
});

export default connect(mapStateToProps)(Home);
