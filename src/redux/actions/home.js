export const TEST = 'TEST';

export const test = (id, name) => {
  const error = !name ? 'Error' : '';

  return {
    type: TEST,
    id, name, error,
  };
};

