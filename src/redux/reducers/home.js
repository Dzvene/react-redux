// import { } from '../actions/home';

const initialState = {
  test: { id: '', name: '' },
  error: '',
};

const homeReducer = (state = initialState, action) => {
  switch (action.type) {
    default:
      return state;
  }
};

export const HomeReducer = {
  home: homeReducer,
};
