import { Route, IndexRoute } from 'react-router';

import App from './containers/app/App';
import Home from './containers/pages/home';

export default (
  <Route
    component={App}
    path={'/'}
  >
    <IndexRoute component={Home}/>
    <Route
      component={Home}
      path={'/home'}
    />
    <Route
      component={Home}
      path="*"
    />
  </Route>
);
