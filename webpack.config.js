const webpack = require('webpack')
const path = require('path')

const autoprefixer = require('autoprefixer');
const StyleLintPlugin = require('stylelint-webpack-plugin');

const NODE_ENV = process.env.NODE_ENV || 'development';

module.exports = {
    entry: [
        './src/index.js'
    ],
    output: {
        path: __dirname + '/dist',
        filename: 'bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.less/,
                use: [{
                    loader: "style-loader" // creates style nodes from JS strings
                }, {
                    loader: "css-loader" // translates CSS into CommonJS
                }, {
                    loader: "less-loader" // compiles Sass to CSS
                }]
            },
            {
                test: /\.js/,
                use: ['babel-loader?cacheDirectory'],
                exclude: /node_modules/,
            },
        ],
    },
    plugins: [
        new webpack.ProvidePlugin({
          React: 'react'
        }),
        new webpack.LoaderOptionsPlugin({
          test: /\.less$/,
          options: {
            postcss: [
              autoprefixer(
                {
                browsers: [
                  'last 3 version',
                  'ie >= 10',
                ],
              }),
            ],
          },
        }),
        new webpack.optimize.OccurrenceOrderPlugin(),
        new webpack.NoEmitOnErrorsPlugin(),
        new StyleLintPlugin({
            configFile: '.stylelintrc',
            context: './src/',
            files: '**/*.?(less|css)',
            failOnError: false,
        }),
        new webpack.DefinePlugin({
            '__DEVTOOLS__': false,
            'process.env': {
                NODE_ENV: JSON.stringify('development'),
            },
        }),
        new webpack.LoaderOptionsPlugin({ options: { postcss: [autoprefixer] } })
    ],
    devServer: {
        host: 'localhost',
        port: 3008,
        contentBase: __dirname + '/dist',
        inline: true,
        hot: false,
        historyApiFallback: true
    },
};

if (NODE_ENV === 'production') {
    module.exports.plugins.push(
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings:       false,
                drop_console:   true,
                unsafe:         true
            }
        })
    );
}
